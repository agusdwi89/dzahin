<!DOCTYPE html>
<html>
<head>
	<?=$this->load->view('fe_porto/header_css_i');?>

	<script type="text/javascript">
		var CLOG = {};
	</script>
</head>
<body data-spy="scroll" data-target=".header-nav-main nav" data-offset="65">
	
	<?=$this->load->view('fe_porto/section/header');?>

	<div role="main" class="main">

		<?=$this->load->view('fe_porto/section/top_overview');?>		

		<?=$this->load->view('fe_porto/section/mid_description');?>

		<?=$this->load->view('fe_porto/section/theme');?>

		<?=$this->load->view('fe_porto/section/faq');?>
		
		<?=$this->load->view('fe_porto/section/footer');?>

	</div>

	<?=$this->load->view('fe_porto/footer_js_i');?>

	<script type="text/javascript">
		$(function(){
			$('#modalChooseTheme').on('show.bs.modal', function (e) {
				var source = $(e.relatedTarget);
				$("#modalTheme").attr('src',source.data('img'));
				$("#modalThemeBtn1").attr('href',source.data('url_cust'));
				$("#modalThemeBtn2").attr('href',source.data('url_prev'));
			})
		})
	</script>

</body>
</html>