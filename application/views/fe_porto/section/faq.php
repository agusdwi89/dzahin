<section id="reviews" class="section bg-color-tertiary m-0">
	<div class="container">
		<div class="row">
			<div class="col text-center">
				<h2 class="text-color-light">Reviews</h2>
				<div class="owl-carousel custom-arrows-style-1 _custom-nav-bottom _big" data-plugin-options="{'items': 2, 'responsive': {'479': {'items': 1}, '979': {'items': 2}, '1199': {'items': 2}}, 'margin': 80, 'loop': false, 'dots': false, 'nav': true}">
					<div>
						<div class="custom-review text-left">
							<h4 class="text-color-light">
								Robert Doe
								<span class="stars text-color-primary">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</span>
							</h4>
							<p class="custom-color-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae ipsa, molestiae, modi cupiditate impedit, laborum excepturi rem maiores iusto suscipit rerum ratione consequatur. Neque id consectetur reiciendis, dolore animi.</p>
						</div>
						<div class="custom-review text-left">
							<h4 class="text-color-light">
								Albert Manjo
								<span class="stars text-color-primary">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</span>
							</h4>
							<p class="custom-color-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam quod, recusandae est aperiam autem repellat nostrum quibusdam, placeat quidem maxime harum tempore explicabo iure aut sunt impedit laborum natus.</p>
						</div>
					</div>
					<div>
						<div class="custom-review text-left">
							<h4 class="text-color-light">
								Mathew Joseph
								<span class="stars text-color-primary">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</span>
							</h4>
							<p class="custom-color-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut tenetur minima, blanditiis nesciunt deleniti sunt, omnis. Ea itaque facilis voluptates dolorem, consectetur dignissimos laborum quam minus eligendi, porro quis!</p>
						</div>
						<div class="custom-review text-left">
							<h4 class="text-color-light">
								Mariya Sino
								<span class="stars text-color-primary">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</span>
							</h4>
							<p class="custom-color-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla inventore hic praesentium aperiam voluptatum cumque in ducimus quaerat libero sunt corporis, similique enim, harum iure nisi numquam aliquam magni!</p>
						</div>
					</div>
					<div>
						<div class="custom-review text-left">
							<h4 class="text-color-light">
								Robert Doe
								<span class="stars text-color-primary">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</span>
							</h4>
							<p class="custom-color-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae ipsa, molestiae, modi cupiditate impedit, laborum excepturi rem maiores iusto suscipit rerum ratione consequatur. Neque id consectetur reiciendis, dolore animi.</p>
						</div>
						<div class="custom-review text-left">
							<h4 class="text-color-light">
								Albert Manjo
								<span class="stars text-color-primary">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</span>
							</h4>
							<p class="custom-color-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam quod, recusandae est aperiam autem repellat nostrum quibusdam, placeat quidem maxime harum tempore explicabo iure aut sunt impedit laborum natus.</p>
						</div>
					</div>
					<div>
						<div class="custom-review text-left">
							<h4 class="text-color-light">
								Mathew Joseph
								<span class="stars text-color-primary">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</span>
							</h4>
							<p class="custom-color-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut tenetur minima, blanditiis nesciunt deleniti sunt, omnis. Ea itaque facilis voluptates dolorem, consectetur dignissimos laborum quam minus eligendi, porro quis!</p>
						</div>
						<div class="custom-review text-left">
							<h4 class="text-color-light">
								Mariya Sino
								<span class="stars text-color-primary">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</span>
							</h4>
							<p class="custom-color-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla inventore hic praesentium aperiam voluptatum cumque in ducimus quaerat libero sunt corporis, similique enim, harum iure nisi numquam aliquam magni!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="faq" class="section bg-color-light m-0">
	<div class="container">
		<div class="row text-center mb-3">
			<div class="col-lg-8 m-auto">
				<h2 class="custom-bar _center text-color-dark">pertanyaan umum</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur repudiandae et saepe, veniam aliquam molestias quod, quasi minus sequi, doloremque dolorum eaque similique eligendi omnis ea!</p>
			</div>
		</div>
		<div class="row mb-5">
			<div class="col-lg-10 m-auto">
				<div class="accordion custom-accordion-style-1" id="accordion">
					<div class="card card-default">
						<div class="card-header">
							<h4 class="card-title m-0">
								<a class="accordion-toggle text-color-dark" data-toggle="collapse" data-parent="#accordion" href="#collapse1One">
									Lorem ipsum dolor sit amet, consectetur adipisicing ?
								</a>
							</h4>
						</div>
						<div id="collapse1One" class="collapse show">
							<div class="card-body">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque eos laborum non molestiae dignissimos accusamus architecto amet beatae eveniet omnis. Animi eos dolore totam dolorum magni impedit optio alias qui nobis dicta deserunt ipsum porro veritatis incidunt ullam ducimus, velit non voluptate a. Rem aut vel necessitatibus officiis, sit quo, at fugit labore minima obcaecati reprehenderit ipsum.</p>
							</div>
						</div>
					</div>
					<div class="card card-default">
						<div class="card-header">
							<h4 class="card-title m-0">
								<a class="accordion-toggle text-color-dark collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1Two">
									Lorem ipsum dolor sit amet, consectetur adipisicing ?
								</a>
							</h4>
						</div>
						<div id="collapse1Two" class="collapse">
							<div class="card-body">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque eos laborum non molestiae dignissimos accusamus architecto amet beatae eveniet omnis. Animi eos dolore totam dolorum magni impedit optio alias qui nobis dicta deserunt ipsum porro veritatis incidunt ullam ducimus, velit non voluptate a. Rem aut vel necessitatibus officiis, sit quo, at fugit labore minima obcaecati reprehenderit ipsum.</p>
							</div>
						</div>
					</div>
					<div class="card card-default">
						<div class="card-header">
							<h4 class="card-title m-0">
								<a class="accordion-toggle text-color-dark collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1Three">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit ?
								</a>
							</h4>
						</div>
						<div id="collapse1Three" class="collapse">
							<div class="card-body">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque eos laborum non molestiae dignissimos accusamus architecto amet beatae eveniet omnis. Animi eos dolore totam dolorum magni impedit optio alias qui nobis dicta deserunt ipsum porro veritatis incidunt ullam ducimus, velit non voluptate a. Rem aut vel necessitatibus officiis, sit quo, at fugit labore minima obcaecati reprehenderit ipsum.</p>
							</div>
						</div>
					</div>
					<div class="card card-default">
						<div class="card-header">
							<h4 class="card-title m-0">
								<a class="accordion-toggle text-color-dark collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1Four">
									Lorem ipsum dolor sit amet, consectetur ?
								</a>
							</h4>
						</div>
						<div id="collapse1Four" class="collapse">
							<div class="card-body">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque eos laborum non molestiae dignissimos accusamus architecto amet beatae eveniet omnis. Animi eos dolore totam dolorum magni impedit optio alias qui nobis dicta deserunt ipsum porro veritatis incidunt ullam ducimus, velit non voluptate a. Rem aut vel necessitatibus officiis, sit quo, at fugit labore minima obcaecati reprehenderit ipsum.</p>
							</div>
						</div>
					</div>
					<div class="card card-default">
						<div class="card-header">
							<h4 class="card-title m-0">
								<a class="accordion-toggle text-color-dark collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1Five">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum ?
								</a>
							</h4>
						</div>
						<div id="collapse1Five" class="collapse">
							<div class="card-body">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque eos laborum non molestiae dignissimos accusamus architecto amet beatae eveniet omnis. Animi eos dolore totam dolorum magni impedit optio alias qui nobis dicta deserunt ipsum porro veritatis incidunt ullam ducimus, velit non voluptate a. Rem aut vel necessitatibus officiis, sit quo, at fugit labore minima obcaecati reprehenderit ipsum.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col">
				<p class="text-color-dark custom-font-secondary text-4 mb-0">Anda punya <strong>Pertanyaan </strong> lain ?</p>
				<p class="text-color-dark custom-font-secondary text-2 mb-5">jangan ragu untuk whatsapp admin kami</p>
				<a href="#" class="btn btn-outline btn-primary text-color-dark custom-btn-style-2">GO TO WHATSAPP</a>
			</div>
		</div>
	</div>
</section>

<div class="p-relative">
	<section id="downloads" class="section section-parallax bg-color-primary m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="<?=base_url()?>assets/feporto/img/demos/app-landing/parallax/downloads-parallax.png">
		<div class="container">
			<div class="row">
				<div class="col-lg-7">
					<h2 class="custom-bar _left _color-light text-color-light">Downloads</h2>
					<p class="text-color-light custom-font-secondary text-4 mb-0">App available for <strong>Android, Iphone and Windows Phone.</strong></p>
					<p class="text-2 text-color-light custom-font-secondary mb-4 pb-3">Also available on the Amazone App Store and Gallery App Store.</p>
					<div class="row">
						<div class="col-sm-4 text-center">
							<a href="#" class="text-decoration-none" target="_blank" title="Download on Google Play">
								<img src="<?=base_url()?>assets/feporto/img/demos/app-landing/buttons/android-download.png" alt class="custom-shadow-on-hover custom-xs-image-center img-fluid" />
							</a>
						</div>
						<div class="col-sm-4 text-center">
							<a href="#" class="text-decoration-none" target="_blank" title="Download on App Store">
								<img src="<?=base_url()?>assets/feporto/img/demos/app-landing/buttons/apple-download.png" alt class="custom-shadow-on-hover custom-xs-image-center img-fluid" />
							</a>
						</div>
						<div class="col-sm-4 text-center">
							<a href="#" class="text-decoration-none" target="_blank" title="Download on Windows Phone Store">
								<img src="<?=base_url()?>assets/feporto/img/demos/app-landing/buttons/windows-download.png" alt class="custom-shadow-on-hover custom-xs-image-center img-fluid" />
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<img src="<?=base_url()?>assets/feporto/img/demos/app-landing/product/downloads-product-image-1.png" data-appear-animation="fadeInRight" data-appear-animation-delay="100" data-plugin-options="{'accY': 200}" alt="" class="custom-product-image-pos-2 img-fluid d-none d-lg-block" />
	<img src="<?=base_url()?>assets/feporto/img/demos/app-landing/product/downloads-product-image-2.png" data-appear-animation="fadeInRight" data-appear-animation-delay="300" data-plugin-options="{'accY': 200}" alt="" class="custom-product-image-pos-2 _litle-small img-fluid d-none d-lg-block" />
</div>