<footer id="footer" class="bg-color-light">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<h4 class="text-color-dark mb-0"><strong>Dzahin.com</strong> &nbsp;Undangan Pernikahan Digital</h4>
				<p class="text-color-dark custom-font-secondary text-2 mb-0 mb-lg-4">Buat undangan online pernikahanmu sekarang.</p>
			</div>
			<div class="col-lg-6">
				<div class="newsletter custom-newsletter-style-1">

				</div>
			</div>
		</div>
	</div>
	<div class="footer-copyright bg-color-light pb-5">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 text-center text-md-left">
					<span class="copyright-text">
						&copy; Copyright 2019. All Rights Reserved.
						<ul class="social-icons custom-social-icons-style-1 _colored">
							<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-square"></i></a></li>
							<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter-square"></i></a></li>
							<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin"></i></a></li>
						</ul>
					</span>
				</div>
				<div class="col-lg-6">
					<nav>
						<ul class="nav nav-pills float-right" id="footerNav">
							<li class="nav-item">
								<a class="nav-link text-color-dark" href="#overview" data-hash>
									OVERVIEW
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-color-dark" href="#" target="_blank" title="Go to Community">
									COMMUNITY
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-color-dark" href="mailto:you@domain.com" title="Contact Us">
									CONTACT US
								</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
</footer>