<header id="header" class="header-transparent header-transparent-dark-bottom-border header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
	<div class="header-body border-top-0 bg-dark box-shadow-none">
		<div class="header-container container">
			<div class="header-row">
				<div class="header-column">
					<div class="header-row">
						<div class="header-logo">
							<a href="demo-app-landing.html">
								<img alt="Porto" width="101" height="23" src="<?=base_url()?>assets/feporto/img/demos/app-landing/logo.png">
							</a>
						</div>
					</div>
				</div>
				<div class="header-column justify-content-end">
					<div class="header-row">
						<div class="header-nav header-nav-links header-nav-dropdowns-dark header-nav-light-text order-2 order-lg-1">
							<div class="header-nav-main header-nav-main-mobile-dark header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-2 header-nav-main-sub-effect-1">
								<nav class="collapse">
									<ul class="nav nav-pills" id="mainNav">
										<li>
											<a class="nav-link active" href="#overview" data-hash>
												OVERVIEW
											</a>
										</li>
										<li>
											<a class="nav-link" href="#how-it-work" data-hash data-hash-offset="62">
												HOW IT WORK
											</a>
										</li>
										<li>
											<a class="nav-link" href="#key-features" data-hash data-hash-offset="62">
												KEY FEATURES
											</a>
										</li>
										<li>
											<a class="nav-link" href="#reviews" data-hash data-hash-offset="62">
												REVIEWS
											</a>
										</li>
										<li>
											<a class="nav-link" href="#faq" data-hash data-hash-offset="62">
												FAQ'S
											</a>
										</li>
										<li>
											<a class="nav-link" href="#downloads" data-hash>
												DOWNLOADS
											</a>
										</li>
									</ul>
								</nav>
							</div>
							<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
								<i class="fas fa-bars"></i>
							</button>
						</div>
						<div class="order-1 order-lg-2">
							<div class="d-inline-flex">
								<ul class="header-social-icons social-icons custom-social-icons-style-1 _white d-none d-sm-block">
									<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-square"></i></a></li>
									<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter-square"></i></a></li>
									<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>