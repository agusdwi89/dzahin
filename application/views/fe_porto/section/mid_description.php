<div id="home-intro" class="home-intro custom-home-intro bg-color-tertiary m-0">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-sm-8">
				<p class="text-color-light m-0">
					Jadikan undangan pernikahanmu online
					<span class="pt-0 text-2">Gampang dibagikan, lebih mudah diakses teman anda</span>
				</p>
			</div>
			<div class="col-lg-4 col-sm-4"  id="pilih-tema">
				<a href="#downloads" class="btn btn-primary custom-btn-style-1 text-uppercase font-weight-semibold float-md-right mt-1" data-hash data-hash-offset="62"><i class="icon-cloud-download icons mr-3"></i>PILIH TEMA</a>
			</div>
		</div>
	</div>
</div>

<section id="how-it-work" class="section bg-color-light m-0">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8 col-lg-5">
				<img src="<?=base_url()?>assets/feporto/img/demos/app-landing/product/how-works-product-image-2.png" data-appear-animation="fadeInLeft" data-appear-animation-delay="300" data-plugin-options="{'accY': 200}" alt class="appear-animation custom-product-image-pos-1 _relative img-fluid" />
				<img src="<?=base_url()?>assets/feporto/img/demos/app-landing/product/how-works-product-image-1.png" data-appear-animation="fadeInLeft" data-appear-animation-delay="100" data-plugin-options="{'accY': 10}" alt class="appear-animation custom-product-image-pos-1 _absolute img-fluid" />
			</div>
			<div class="col-md-10 col-lg-7 text-center text-lg-left">
				<h2 class="custom-bar _left text-color-dark">apa itu Undangan Digital ?</h2>
				<p class="mb-5 pb-5">Undangan digital adalah undangan pernikahan yang disajikan dalam bentuk online, bisa di akses dari mana saja, bahkan dari handphone. Undangan digital lebih interaktif, dapat menceritakna perjalanan cinta anda & pasangan. Mudah dibuat, praktis, mudah dibagikan tidak perlu kirim-kirim undangan ke teman jauh</p>
			</div>
		</div>
	</div>
</section>

<section id="counter" class="section section-parallax section-text-light section-center m-0 pt-4 pb-4" data-plugin-parallax="" data-plugin-options="{'speed': 1.5}" data-image-src="<?=base_url()?>assets/feporto/img/parallax-event.jpg" style="position: relative; overflow: hidden;"><div class="parallax-background" style="background-image: url(&quot;img/demos/event/counter/parallax-event.jpg&quot;); background-size: cover; position: absolute; top: 0px; left: 0px; width: 100%; height: 180%; transform: translate3d(0px, -94.7946px, 0px); background-position-x: 50%;"></div>
	<div class="container mt-4 mb-4">
		<div class="row mt-2 mb-2 counters counters-text-light">
			<div class="col-12 col-lg-3 col-sm-6">
				<div class="counter mb-4 mt-4">
					<img src="img/demos/event/counter/col1.png" class="img-fluid m-auto" alt="">
					<strong class="text-color-light mt-3 mb-1" data-to="30000" data-append="+">30000+</strong>
					<label class="text-color-light font-weight-normal text-uppercase text-center custom-font-size-2">Daily Visitors</label>
				</div>
			</div>
			<div class="col-12 col-lg-3 col-sm-6">
				<div class="counter mb-4 mt-4">
					<img src="img/demos/event/counter/col2.png" class="img-fluid m-auto" alt="">
					<strong class="text-color-light mt-3 mb-1" data-to="15">15</strong>
					<label class="text-color-light font-weight-normal text-uppercase text-center custom-font-size-2">Years</label>
				</div>
			</div>
			<div class="col-12 col-lg-3 col-sm-6">
				<div class="counter mb-4 mt-4">
					<img src="img/demos/event/counter/col3.png" class="img-fluid m-auto" alt="">
					<strong class="text-color-light mt-3 mb-1" data-to="33">33</strong>
					<label class="text-color-light font-weight-normal text-uppercase text-center custom-font-size-2">Awards</label>
				</div>
			</div>
			<div class="col-12 col-lg-3 col-sm-6">
				<div class="counter mb-4 mt-4">
					<img src="img/demos/event/counter/col4.png" class="img-fluid m-auto" alt="">
					<strong class="text-color-light mt-3 mb-1" data-to="178">178</strong>
					<label class="text-color-light font-weight-normal text-uppercase text-center custom-font-size-2">Successful Stories</label>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="key-features" class="section bg-color-light m-0">
	<div class="container">
		<div class="row text-center mb-3">
			<div class="col-lg-8 m-auto">
				<h2 class="custom-bar _center text-color-dark">Layanan Terbaik</h2>
				<p>Dzahin.com undangan pernikahan online memberikan layanan terbaik untuk kepuasan pelanggan</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-lg-6 m-auto">
				<div class="feature-box feature-box-style-3 reverse custom-feature-box-style-1">
					<div class="feature-box-icon">
						<i class="fas fa-tv custom-icon-fix-pos"></i>
					</div>
					<div class="feature-box-info text-2">
						<h4 class="text-color-dark pt-1">Desain Kekinian</h4>
						<p class="mb-4">Desain undangan menyesuaikan kepribadian anda</p>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-lg-6 m-auto">
				<div class="feature-box feature-box-style-3 custom-feature-box-style-1">
					<div class="feature-box-icon">
						<i class="fas fa-sync"></i>
					</div>
					<div class="feature-box-info text-2">
						<h4 class="text-color-dark pt-1">Ceritakan Perjalanan Cinta</h4>
						<p class="mb-4">Bagikan pengalaman perjalanan cinta ke teman-teman undangan</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-lg-6 m-auto">
				<div class="feature-box feature-box-style-3 reverse custom-feature-box-style-1">
					<div class="feature-box-icon">
						<i class="fas fa-user-plus custom-icon-fix-pos"></i>
					</div>
					<div class="feature-box-info text-2">
						<h4 class="text-color-dark pt-1">Interaktif & mudah dibagikan</h4>
						<p class="mb-4">Bagikan link ke teman-teman & biarkan mereka melihat perjalanan cinta kalian</p>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-lg-6 m-auto">
				<div class="feature-box feature-box-style-3 custom-feature-box-style-1">
					<div class="feature-box-icon">
						<i class="far fa-life-ring"></i>
					</div>
					<div class="feature-box-info text-2">
						<h4 class="text-color-dark pt-1">Gallery Foto</h4>
						<p class="mb-4">Unggah lebih banyak foto-foto prewedding dalam undangan online.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-lg-6 m-auto">
				<div class="feature-box feature-box-style-3 reverse custom-feature-box-style-1">
					<div class="feature-box-icon">
						<i class="fas fa-users"></i>
					</div>
					<div class="feature-box-info text-2">
						<h4 class="text-color-dark pt-1">Bagikan Ke siapa saja</h4>
						<p class="mb-4">Anda bisa bagikan ke teman-teman dimanapun mereka bereda, kabar baik sudah sewajarnya dibagikan ke banyak orang </p>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-lg-6 m-auto">
				<div class="feature-box feature-box-style-3 custom-feature-box-style-1">
					<div class="feature-box-icon">
						<i class="fas fa-paint-brush"></i>
					</div>
					<div class="feature-box-info text-2">
						<h4 class="text-color-dark pt-1">Mudah & Efisien</h4>
						<p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="home-intro" id="home-intro">
	<div class="container">

		<div class="row align-items-center">
			<div class="col text-center">
				<p class="mb-0">
					The fastest way to grow your business with the leader in <span class="highlighted-word highlighted-word-animation-1 text-color-primary font-weight-semibold text-5">Technology</span>
					<span>Check out our options and features included.</span>
				</p>
			</div>
		</div>

	</div>
</div>

<div class="container">
	<div class="row pt-4">
		<div class="col-lg-8 pb-2">

			<div class="process process-vertical pt-4">
				<div class="process-step appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200" style="animation-delay: 200ms;">
					<div class="process-step-circle">
						<strong class="process-step-circle-content">1</strong>
					</div>
					<div class="process-step-content">
						<h4 class="mb-1 text-4 font-weight-bold">Mulai Mendaftar</h4>
						<p class="mb-0">Daftar di dzahin.com, bisa menggunakan email / facebook untuk membuat undangan online</p>
					</div>
				</div>
				<div class="process-step appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400" style="animation-delay: 400ms;">
					<div class="process-step-circle">
						<strong class="process-step-circle-content">2</strong>
					</div>
					<div class="process-step-content">
						<h4 class="mb-1 text-4 font-weight-bold">Isi Detail Undangan</h4>
						<p class="mb-0">Mulai isi data-data terkait pernikahan, tanggal pernikahan, lokasi, detail acara, & foto-foto pra wedding</p>
					</div>
				</div>
				<div class="process-step appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="600" style="animation-delay: 600ms;">
					<div class="process-step-circle">
						<strong class="process-step-circle-content">3</strong>
					</div>
					<div class="process-step-content">
						<h4 class="mb-1 text-4 font-weight-bold">Pra Tinjau Undangan</h4>
						<p class="mb-0">Tinjau undangan sebelum dibagikan ke teman-teman, sampai disini tidak ada biaya yang anda keluarkan</p>
					</div>
				</div>
				<div class="process-step pb-4 mb-2 appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="800" style="animation-delay: 800ms;">
					<div class="process-step-circle">
						<strong class="process-step-circle-content">4</strong>
					</div>
					<div class="process-step-content">
						<h4 class="mb-1 text-4 font-weight-bold">Langkah Terakhir</h4>
						<p class="mb-0">Undangan sudah sempurna, anda bisa bagikan ke teman-teman anda setelah melakukan pembayaran.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4 px-5 pt-2 appear-animation animated fadeIn appear-animation-visible" data-appear-animation="fadeIn" data-appear-animation-delay="800" style="animation-delay: 800ms;">
			<div class="testimonial testimonial-style-7 testimonial-primary py-5 mt-4">
				<blockquote>
					<p class="text-6 line-height-9 mb-0 mt-4">Dapatkan uji coba gratis untuk melihat undangan online sebelum anda bagikan ke teman-teman</p>
				</blockquote>
			</div>
		</div>
	</div>
</div>