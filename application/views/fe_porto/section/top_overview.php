<section id="overview" class="section custom-bg-color-1 custom-background-style-1 m-0">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-lg-8">
				<div class="custom-top-title-box">
					<h1 class="text-color-light">Undangan Digital</h1>
					<span class="text-color-light font-weight-semibold mb-5">Bikin undangan online dari genggaman anda</span>
					<a href="#downloads" class="btn custom-btn-style-1 text-color-light mb-5" data-hash>Bikin Sekarang</a>
					<a href="#key-features" class="btn btn-primary custom-btn-style-1 _borders text-color-light ml-2 mb-5" data-hash data-hash-offset="62">Cari tahu</a>
				</div>
			</div>
			<div class="col-8 col-md-4 col-lg-4 mx-auto">
				<div class="owl-carousel custom-arrows-style-1 custom-left-pos-1 custom-background-1 m-0" data-plugin-options="{'items': 1, 'loop': true, 'dots': false, 'nav': true, 'autoplay': true, 'autoplayTimeout': 3000}">
					<div>
						<img src="<?=base_url()?>assets/feporto/img/demos/app-landing/product/overview-carousel-1.jpg" alt class="img-fluid" />
					</div>	
					<div>
						<img src="<?=base_url()?>assets/feporto/img/demos/app-landing/product/overview-carousel-2.jpg" alt class="img-fluid" />
					</div>	
					<div>
						<img src="<?=base_url()?>assets/feporto/img/demos/app-landing/product/overview-carousel-3.jpg" alt class="img-fluid" />
					</div>
				</div>
			</div>
		</div>
	</div>
</section>