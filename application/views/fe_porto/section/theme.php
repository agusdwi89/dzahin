<section class="section border-0 m-0 pb-3">
	<div class="container container-lg">
		<div class="row text-center mb-3">
			<div class="col-lg-8 m-auto">
				<h2 class="custom-bar _center text-color-dark">Pilihan Undangan</h2>
				<p>Pilih tema undangan pernikahan anda sekarang, sesuaikan dengan style & warna anda</p>
			</div>
		</div>
		<div class="row pb-1">
			<?php foreach ($theme->result() as $r): ?>
				<div class="col-sm-6 col-lg-4 mb-4 pb-2">
					<a href="#" data-toggle="modal" data-url_cust="<?=base_url()?>customer/choose/<?=$r->id;?>/<?=url_title($r->name)?>" data-url_prev="<?=base_url()?>preview/<?=$r->id;?>/<?=url_title($r->name)?>" data-target="#modalChooseTheme"  data-name = "<?=$r->name?>" data-description = "<?=$r->description?>" data-directory = "<?=$r->directory?>" data-price = "<?=$r->price?>" data-img = "<?=base_url()?>assets/feporto/theme-image/<?=$r->img?>">
						<span class="thumb-info thumb-info-no-borders thumb-info-bottom-info thumb-info-bottom-info-dark thumb-info-bottom-info-show-more thumb-info-no-zoom border-radius-0">
							<span class="thumb-info-wrapper thumb-info-wrapper-opacity-6">
								<img src="<?=base_url()?>assets/feporto/theme-image/thumb/<?=$r->img;?>" class="img-fluid" alt="">
								<span class="thumb-info-title bg-transparent p-4">
									<span class="thumb-info-type bg-color-primary px-2 mb-1">Rp. <?=$r->price;?>,-</span>
									<span class="thumb-info-inner line-height-1 text-4 font-weight-bold mt-1"><?=$r->name;?></span>
									<span class="thumb-info-show-more-content">
										<p class="mb-0 text-1 line-height-9 mb-1 mt-2 text-light opacity-5"><?=$r->description;?></p>
									</span>
								</span>
							</span>
						</span>
					</a>
				</div>
			<?php endforeach ?>
		</div>
	</div>
</section>


<div>

	<button class="btn btn-modern btn-primary" data-toggle="modal" data-target="#modalChooseTheme">
		Launch Default Modal
	</button>

	<div class="modal fade" id="modalChooseTheme" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="defaultModalLabel">Ini Judul</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<img id="modalTheme">
					<div id="modalThemeDesc">
						<span>Rp.300.000</span>
						<p>asjda alsjkda kslajd;sajd asdkja askdj;asljd as;dja sd;ajd ;ajsd </p>
					</div>
				</div>
				<div class="modal-footer">
					<a id="modalThemeBtn1" href="#" class="btn btn-primary mb-2">Pilih Tema</a>
					<a target="_blank" id="modalThemeBtn2" href="#" class="btn btn-secondary mb-2">Preview</a>
				</div>
			</div>
		</div>
	</div>
</div>