<!-- Basic -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">	
<meta name="theme-color" content="#e637a0">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

<title>Demo App Landing | Porto - Responsive HTML5 Template 7.1.0</title>	

<meta name="keywords" content="HTML5 Template" />
<meta name="description" content="Porto - Responsive HTML5 Template">
<meta name="author" content="okler.net">

<!-- Favicon -->
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

<!-- Web Fonts  -->
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css">

<!-- Vendor CSS -->
<link rel="stylesheet" href="<?=base_url()?>assets/feporto/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/feporto/vendor/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/feporto/vendor/animate/animate.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/feporto/vendor/simple-line-icons/css/simple-line-icons.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/feporto/vendor/owl.carousel/assets/owl.carousel.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/feporto/vendor/owl.carousel/assets/owl.theme.default.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/feporto/vendor/magnific-popup/magnific-popup.min.css">

<!-- Theme CSS -->
<link rel="stylesheet" href="<?=base_url()?>assets/feporto/css/theme.css">
<link rel="stylesheet" href="<?=base_url()?>assets/feporto/css/theme-elements.css">
<link rel="stylesheet" href="<?=base_url()?>assets/feporto/css/theme-blog.css">
<link rel="stylesheet" href="<?=base_url()?>assets/feporto/css/theme-shop.css">

<!-- Current Page CSS -->
<link rel="stylesheet" href="<?=base_url()?>assets/feporto/vendor/rs-plugin/css/settings.css">
<link rel="stylesheet" href="<?=base_url()?>assets/feporto/vendor/rs-plugin/css/layers.css">
<link rel="stylesheet" href="<?=base_url()?>assets/feporto/vendor/rs-plugin/css/navigation.css">
		
<!-- Demo CSS -->
<link rel="stylesheet" href="<?=base_url()?>assets/feporto/css/demos/demo-app-landing.css">

<!-- Theme Custom CSS -->
<link rel="stylesheet" href="<?=base_url()?>assets/feporto/css/custom.css">

<!-- Theme Custom CSS -->
<link rel="stylesheet" href="<?=base_url()?>assets/feporto/css/skins/skin2.css">

<!-- Head Libs -->
<script src="<?=base_url()?>assets/feporto/vendor/modernizr/modernizr.min.js"></script>