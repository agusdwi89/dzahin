<!-- Vendor -->
<script src="<?=base_url()?>assets/feporto/vendor/jquery/jquery.min.js"></script>		
<script src="<?=base_url()?>assets/feporto/vendor/jquery.appear/jquery.appear.min.js"></script>		
<script src="<?=base_url()?>assets/feporto/vendor/jquery.easing/jquery.easing.min.js"></script>		
<script src="<?=base_url()?>assets/feporto/vendor/jquery.cookie/jquery.cookie.min.js"></script>		
<script src="<?=base_url()?>assets/feporto/vendor/popper/umd/popper.min.js"></script>		
<script src="<?=base_url()?>assets/feporto/vendor/bootstrap/js/bootstrap.min.js"></script>		
<script src="<?=base_url()?>assets/feporto/vendor/common/common.min.js"></script>		
<script src="<?=base_url()?>assets/feporto/vendor/jquery.validation/jquery.validate.min.js"></script>		
<script src="<?=base_url()?>assets/feporto/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>		
<script src="<?=base_url()?>assets/feporto/vendor/jquery.gmap/jquery.gmap.min.js"></script>		
<script src="<?=base_url()?>assets/feporto/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>		
<script src="<?=base_url()?>assets/feporto/vendor/isotope/jquery.isotope.min.js"></script>		
<script src="<?=base_url()?>assets/feporto/vendor/owl.carousel/owl.carousel.min.js"></script>		
<script src="<?=base_url()?>assets/feporto/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>		
<script src="<?=base_url()?>assets/feporto/vendor/vide/jquery.vide.min.js"></script>		
<script src="<?=base_url()?>assets/feporto/vendor/vivus/vivus.min.js"></script>
	
<!-- Theme Base, Components and Settings -->
<script src="<?=base_url()?>assets/feporto/js/theme.js"></script>
	
<!-- Current Page Vendor and Views -->
<script src="<?=base_url()?>assets/feporto/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>		
<script src="<?=base_url()?>assets/feporto/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- Current Page Vendor and Views -->
<script src="<?=base_url()?>assets/feporto/js/views/view.contact.js"></script>

<!-- Demo -->
<script src="<?=base_url()?>assets/feporto/js/demos/demo-app-landing.js"></script>
	
<!-- Theme Custom -->
<script src="<?=base_url()?>assets/feporto/js/custom.js"></script>
	
<!-- Theme Initialization Files -->
<script src="<?=base_url()?>assets/feporto/js/theme.init.js"></script>