<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="images/favicon.png">
<title>Jazy - eCommerce Mobile Template</title>

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

<link rel="stylesheet" href="<?=base_url()?>assets/css/materialize.css">
<link rel="stylesheet" href="<?=base_url()?>assets/css/loaders.css">
<link rel="stylesheet" href="<?=base_url()?>assets/css/line-awesome.css">
<link rel="stylesheet" href="<?=base_url()?>assets/css/line-awesome-font-awesome.css">
<link rel="stylesheet" href="<?=base_url()?>assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/css/owl.theme.default.min.css">
<link rel="alternate stylesheet" type="text/css" href="<?=base_url()?>assets/css/style-dark.css" title="dark-mode">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/style.css" title="default">

<script src="<?=base_url()?>assets/js/jquery.min.js"></script>
<script src="<?=base_url()?>assets/js/materialize.js"></script>
<script src="<?=base_url()?>assets/js/owl.carousel.min.js"></script>
<script src="<?=base_url()?>assets/js/styleswitcher.js"></script>
<script src="<?=base_url()?>assets/js/main.js"></script>