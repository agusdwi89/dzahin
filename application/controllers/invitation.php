<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invitation extends CI_Controller {

	public function index()
	{
		$this->load->view('welcome_message');
	}

	function detail($id,$to=""){
		$page = $this->db->get_where('iv_page',array('url'=>$id));
		// debug_array($page);

		if ($page->num_rows() == 1) {
			$page 	= $page->row();				
			$theme	= $this->db->get_where('iv_theme',array('id'=>$page->theme))->row();
		}else{
			show_404();
		}

		$data['detail']		= $page;
		$data['theme_dir']		= $theme->directory;
		$this->load->view("theme/{$theme->directory}/index",$data);
	}
}