/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100137
 Source Host           : localhost:3306
 Source Schema         : undanganku

 Target Server Type    : MySQL
 Target Server Version : 100137
 File Encoding         : 65001

 Date: 05/02/2019 14:13:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for iv_page
-- ----------------------------
DROP TABLE IF EXISTS `iv_page`;
CREATE TABLE `iv_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(100) DEFAULT NULL,
  `theme` int(255) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of iv_page
-- ----------------------------
BEGIN;
INSERT INTO `iv_page` VALUES (1, 'agus-dwi', 1, '2019-02-04 18:21:01', NULL, NULL, 'active');
COMMIT;

-- ----------------------------
-- Table structure for iv_theme
-- ----------------------------
DROP TABLE IF EXISTS `iv_theme`;
CREATE TABLE `iv_theme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `directory` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of iv_theme
-- ----------------------------
BEGIN;
INSERT INTO `iv_theme` VALUES (1, 'zawag', 'zawag', 'zawag.img');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
